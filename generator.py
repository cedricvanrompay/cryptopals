import os
import os.path
import shutil
from pathlib import Path

from nbconvert import HTMLExporter
from jinja2 import Template

def empty_dir(path):
    path = Path(path)

    for child in path.iterdir():
        if child.is_file():
           child.unlink()
        elif child.is_dir():
            shutil.rmtree(child)

def main():
    exporter = HTMLExporter()
    exporter.template_name = 'basic'


    os.makedirs('public', exist_ok=True)
    empty_dir('public/')
    os.mkdir('public/challenges')

    with open('template.html') as f:
        template = Template(f.read())

    for child in Path('challenges/').iterdir():
        if child.is_dir():
            continue

        elif child.suffix != '.ipynb':
            shutil.copy(str(child), 'public/challenges/')

        else:
            (body, resources) = exporter.from_filename(child)
            title = resources['metadata']['name']

            html = template.render(title=title, content=body)

            # must use 'with_suffix' first
            # because '.name' returns a string, not a Path()
            destination = Path('public/challenges/') / child.with_suffix('.html').name

            with open(destination, 'w') as f:
                f.write(html)

    os.mkdir('public/challenges/figures')
    for figure in Path('challenges/figures/').iterdir():
        shutil.copy(str(figure), 'public/challenges/figures/')

    shutil.copy('index.html', 'public/')
    shutil.copy('custom.css', 'public/challenges/')


if __name__ == '__main__':
    main()
