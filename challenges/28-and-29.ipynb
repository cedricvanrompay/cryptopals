{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Challenge 28: Implement a SHA-1 keyed MAC\n",
    "\n",
    "> Find a SHA-1 implementation in the language you code in.\n",
    "> \n",
    "> **Don't cheat. It won't work.**\n",
    "> Do not use the SHA-1 implementation your language already provides\n",
    "> (for instance, don't use the \"Digest\" library in Ruby, or call OpenSSL; in Ruby, you'd want a pure-Ruby SHA-1).\n",
    "> \n",
    "> Write a function to authenticate a message under a secret key by using a secret-prefix MAC, which is simply:\n",
    "> \n",
    ">     SHA1(key || message)\n",
    "> \n",
    "> Verify that you cannot tamper with the message without breaking the MAC you've produced, and that you can't produce a new MAC without knowing the secret key."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from libmatasano import html_test, bxor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Two important remarks:\n",
    "\n",
    "- We know SHA-1 has security issues since 2004 (see [Bruce Schneier's blog post][]).\n",
    "  At the time they were only *theoretical* issues,\n",
    "  meaning that they didn't give a way to actually break it,\n",
    "  but we were pretty sure it would get broken soon.\n",
    "  This is what motivated the switch to SHA-2.\n",
    "  Nowadays SHA-1 is **completely broken**\n",
    "  (see [Leurent and  Peyrin 2020][])\n",
    "- The attack in the next challenge does not use the fact that SHA-1 is broken.\n",
    "  It uses a specific property of SHA-1 due to the way it is built\n",
    "  (“Merkle–Damgård Construction”).\n",
    "  *The same attack would work even with SHA-2*\n",
    "  because SHA-2 has that same property.\n",
    "  \n",
    " Said differently:\n",
    " the attack in the next challenge is not due to a problem in SHA-1\n",
    " (even if SHA-1 is broken as hell)\n",
    " but to a problem in the (bad) MAC algorithm we build on top of SHA-1. \n",
    "  \n",
    "[Bruce Schneier's blog post]: https://www.schneier.com/blog/archives/2005/02/cryptanalysis_o.html\n",
    "[Leurent and  Peyrin 2020]: https://eprint.iacr.org/2020/014"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Implementing SHA-1 in Python\n",
    "\n",
    "The instructions do not explicitely ask us to implement SHA-1 ourselves,\n",
    "but this is a nice opportunity to dip your toes in the waters of hash functions implementation.\n",
    "\n",
    "For the specifications you can use\n",
    "[RFC 3174](https://tools.ietf.org/html/rfc3174)\n",
    "or\n",
    "[NIST FIPS 180-4](https://csrc.nist.gov/publications/detail/fips/180/4/final).\n",
    "\n",
    "For some test data with intermediate values\n",
    "(useful for debugging your implementation)\n",
    "see this page:\n",
    "\n",
    "https://csrc.nist.gov/projects/cryptographic-standards-and-guidelines/example-values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from libmatasano import split_bytes_in_blocks\n",
    "\n",
    "def sha1(msg):\n",
    "    # following RFC 3174\n",
    "    # https://tools.ietf.org/html/rfc3174\n",
    "\n",
    "    # we are prioritizing readability and similarity with the specs\n",
    "    # over optimization\n",
    "\n",
    "    # we are always in big-endian form in SHA1\n",
    "    # (Section 2.c: \"The least significant four bits of the integer are\n",
    "    # represented by the right-most hex digit of the word representation\")\n",
    "\n",
    "    # to use as a bit mask for reduction modulo 2^32\n",
    "    MAX_WORD = 0xFFFFFFFF\n",
    "\n",
    "    # Section 3: Operations on Words\n",
    "\n",
    "    def S(X, n):\n",
    "        'circular left shift (a.k.a \"rotate left\")'\n",
    "        # don't forget reduction modulo 2^32 !\n",
    "        # it is not explicitely written in the formula in the RFC\n",
    "        # (it is in the prose below it though)\n",
    "        return ((X << n) | (X >> (32-n))) & MAX_WORD\n",
    "\n",
    "    # Section 4: Padding\n",
    "\n",
    "    # we are limiting ourselves to messages being byte strings\n",
    "    # even though specification mentions bit strings of any length\n",
    "    assert isinstance(msg, bytes)\n",
    "\n",
    "    # message length in bits\n",
    "    msg_length = len(msg)*8\n",
    "\n",
    "    # we must append a \"1\" bit.\n",
    "    # since we are always working with bytes\n",
    "    # the appended bit will always be at the beginning of the next byte\n",
    "\n",
    "    # computing the number of \"zeroes\" to append\n",
    "    # we need msg_length + 1 + m + 64 = 0 mod 512\n",
    "    # thus m = -(msg_length + 1 + 64) mod 512\n",
    "    m = -(msg_length + 1 + 64) % 512\n",
    "\n",
    "    # m+1 will always be a multiple of 8 in our case\n",
    "    padded_msg = (msg\n",
    "                  + bytes([0b10000000])\n",
    "                  + b'\\x00'*(m//8)\n",
    "                  + msg_length.to_bytes(8, byteorder='big')\n",
    "                 )\n",
    "\n",
    "    words = [int.from_bytes(w, byteorder='big')\n",
    "             for w in split_bytes_in_blocks(padded_msg, 4)]\n",
    "\n",
    "    # \"The padded message will contain 16 * n words\"\n",
    "    n = len(words)/16\n",
    "    assert n.is_integer()\n",
    "    n = int(n)\n",
    "\n",
    "    # \"The padded message is regarded as a sequence of n blocks M(1), M(2), …\"\n",
    "    M = split_bytes_in_blocks(words, 16)\n",
    "\n",
    "    # Section 5: Functions and Constants Used\n",
    "\n",
    "    def f(t, B, C, D):\n",
    "        if 0 <= t <= 19:\n",
    "            return (B & C) | ((~B) & D)\n",
    "        elif 20 <= t <= 39 or 60 <= t <= 79:\n",
    "            return B ^ C ^ D\n",
    "        elif 40 <= t <= 59:\n",
    "            return (B & C) | (B & D) | (C & D)\n",
    "        else:\n",
    "            raise Exception('t must be between 0 and 79 inclusive')\n",
    "\n",
    "    # this could be optimized, for instance with an array\n",
    "    # but this way is closer to how it is described in the specs\n",
    "    def K(t):\n",
    "        if 0 <= t <= 19:\n",
    "            return 0x5A827999\n",
    "        elif 20 <= t <= 39:\n",
    "            return 0x6ED9EBA1\n",
    "        elif 40 <= t <= 59:\n",
    "            return 0x8F1BBCDC\n",
    "        elif 60 <= t <= 79:\n",
    "            return 0xCA62C1D6\n",
    "        else:\n",
    "            raise Exception('t must be between 0 and 79 inclusive')\n",
    "\n",
    "    # Section 6: Computing the Message Digest\n",
    "    # Using \"method 1\" (Section 6.1)\n",
    "\n",
    "    H0 = 0x67452301\n",
    "    H1 = 0xEFCDAB89\n",
    "    H2 = 0x98BADCFE\n",
    "    H3 = 0x10325476\n",
    "    H4 = 0xC3D2E1F0\n",
    "\n",
    "    for i in range(len(M)):\n",
    "        W = M[i]\n",
    "        assert len(W) == 16\n",
    "        \n",
    "        for t in range(16, 80):\n",
    "            W.append( S(W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16],\n",
    "                        n=1) )\n",
    "\n",
    "        A, B, C, D, E = H0, H1, H2, H3, H4\n",
    "\n",
    "        for t in range(80):\n",
    "            TEMP = (S(A, 5) + f(t, B, C, D) + E + W[t] + K(t)) & MAX_WORD\n",
    "\n",
    "            E = D; D = C; C = S(B, 30); B = A; A = TEMP\n",
    "\n",
    "        H0 = (H0 + A) & MAX_WORD\n",
    "        H1 = (H1 + B) & MAX_WORD\n",
    "        H2 = (H2 + C) & MAX_WORD\n",
    "        H3 = (H3 + D) & MAX_WORD\n",
    "        H4 = (H4 + E) & MAX_WORD\n",
    "\n",
    "    result = b''.join(H.to_bytes(4, byteorder='big') for H in [H0, H1, H2, H3, H4])\n",
    "\n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div style=\"border:1px solid #c3e6cb;padding:.75rem 3rem;border-radius:.5rem;font-weight:bold;text-align: center;background-color:#d4edda;color:#155724;border-color:#c3e6cb;\">OK</div>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# testing with the sha1 from hashlib\n",
    "import hashlib\n",
    "import os\n",
    "from random import randint\n",
    "\n",
    "for _ in range(5):\n",
    "    size = randint(3,256)\n",
    "    msg = os.urandom(size)\n",
    "    assert sha1(msg) == hashlib.sha1(msg).digest()\n",
    "    \n",
    "html_test(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Implementing the MAC\n",
    "\n",
    "By far the simplest part.\n",
    "\n",
    "“MAC” is for “Message Authentication Code”,\n",
    "it's the equivalent of signatures for symmetric cryptography\n",
    "(signatures are part of asymmetric cryptography)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def secret_prefix_sha1_mac(msg, key):\n",
    "    return sha1(key + msg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "key = os.urandom(16)\n",
    "msg = b'must not be altered'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'<\\xf3\\x90\\x10\\xfa\\xda\\xc3A\\x17\\x10e\\x12[\\xedHv\\xe5\\xfcp\\x90'"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "secret_prefix_sha1_mac(msg, key)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'}]\\xbbB\\xd0\\xfe\\xe7\\xd5{\\xb7\\x15\\xb7\\x10RvB\\xc8\\xc5\\x06u'"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "secret_prefix_sha1_mac(bxor(msg, b'modification'), key)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'j_I\\x83\\xc0\\x86\\xa4\\x86\\xe1T\\x1b5F\\xf2\\x15\\xfb\\xa1\\xd5-\\xd2'"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "secret_prefix_sha1_mac(msg + b'adding some bytes...', key)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There isn't really a way to \"verify\" that we *cannot* break a MAC.\n",
    "Here we just observed that if you modify the message the output is modified as well.\n",
    "The property a MAC function should satisfy is that\n",
    "someone who doesn't know the key must not be able to create a value `Y`\n",
    "such that `max(modified_message, key) == Y`.\n",
    "Said differently a person knowing the key and receiving a pair `(message, tag)`\n",
    "can make sure that the tag was computed by someone who knows the key as well\n",
    "for this exact message."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Challenge 29: Break a SHA-1 keyed MAC using length extension\n",
    "\n",
    "> Secret-prefix SHA-1 MACs are trivially breakable.\n",
    "> \n",
    "> The attack on secret-prefix SHA1 relies on the fact that you can take the ouput of SHA-1 and use it as a new starting point for SHA-1, thus taking an arbitrary SHA-1 hash and \"feeding it more data\".\n",
    "> \n",
    "> Since the key precedes the data in secret-prefix, any additional data you feed the SHA-1 hash in this fashion will appear to have been hashed with the secret key.\n",
    "> \n",
    "> To carry out the attack, you'll need to account for the fact that SHA-1 is \"padded\" with the bit-length of the message; your forged message will need to include that padding. We call this \"glue padding\". The final message you actually forge will be:\n",
    "> \n",
    ">     SHA1(key || original-message || glue-padding || new-message)\n",
    "> \n",
    "> (where the final padding on the whole constructed message is implied)\n",
    "> \n",
    "> Note that to generate the glue padding, you'll need to know the original bit length of the message; the message itself is known to the attacker, but the secret key isn't, so you'll need to guess at it.\n",
    "> \n",
    "> This sounds more complicated than it is in practice.\n",
    "> \n",
    "> To implement the attack, first write the function that computes the MD padding of an arbitrary message and verify that you're generating the same padding that your SHA-1 implementation is using. This should take you 5-10 minutes.\n",
    "> \n",
    "> Now, take the SHA-1 secret-prefix MAC of the message you want to forge --- this is just a SHA-1 hash --- and break it into 32 bit SHA-1 registers (SHA-1 calls them \"a\", \"b\", \"c\", &c).\n",
    "> \n",
    "> Modify your SHA-1 implementation so that callers can pass in new values for \"a\", \"b\", \"c\" &c (they normally start at magic numbers). With the registers \"fixated\", hash the additional data you want to forge.\n",
    "> \n",
    "> Using this attack, generate a secret-prefix MAC under a secret key (choose a random word from /usr/share/dict/words or something) of the string:\n",
    "> \n",
    ">     \"comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon\"\n",
    "> \n",
    "> Forge a variant of this message that ends with \";admin=true\".\n",
    "> \n",
    "> **This is a very useful attack:** For instance: Thai Duong and Juliano Rizzo, who got to this attack before we did, used it to break the Flickr API.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "original_msg = (b\"comment1=cooking%20MCs;userdata=foo;\"\n",
    "                b\"comment2=%20like%20a%20pound%20of%20bacon\")\n",
    "key = os.urandom(16)\n",
    "\n",
    "mac = sha1(key + original_msg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have the MAC of `original_msg`,\n",
    "that is the SHA1 hash of `key + original_msg`.\n",
    "What we want to have is a valid MAC for a string containing `new_text`,\n",
    "without having the knowledge of `key`.\n",
    "\n",
    "Here is how we are going to do it:\n",
    "the SHA1 function works by keeping a \"state\"\n",
    "that is \"updated\" for each block of 512 bits from the data that must be hashed.\n",
    "When SHA-1 hashes the concatenation X || Y of two strings X and Y,\n",
    "during the processing of the blocks of X\n",
    "the state goes through the same values\n",
    "as if we were hashing just X.\n",
    "\n",
    "The idea of the length-extension attack is then the following:\n",
    "using the hash we have of some unknown string X,\n",
    "we can copy the state the SHA-1 function was in\n",
    "when it returned this hash.\n",
    "Then we can use our \"cloned\" SHA-1 to hash some more bytes Y.\n",
    "We get the hash of X || Y even without knowing X.\n",
    "\n",
    "How hard is it to \"clone the state\"?\n",
    "Not hard at all:\n",
    "in SHA-1 the hash that is returned is\n",
    "the concatenation of registers `H0, H1, H2, H3, H4`,\n",
    "which are... the state registers themselves.\n",
    "I don't know why the instructions mention the `A, B, C, …` registers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(3723479287, 289492558, 2524822031, 820116683, 484465219)"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "H0, H1, H2, H3, H4 = [int.from_bytes(x, byteorder='big')\n",
    "                      for x in split_bytes_in_blocks(mac, blocksize=4)]\n",
    "\n",
    "H0, H1, H2, H3, H4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Good !\n",
    "Cloning should be as simple as initializing a new SHA-1\n",
    "but setting the `H0, H1, H2, H3, H4` values to the values we just recovered\n",
    "instead of the fixed values given in the specification.\n",
    "\n",
    "But there is still something we did not take into account: padding.\n",
    "\n",
    "First, we must alter the way our \"cloned SHA-1 function\"\n",
    "does padding:\n",
    "we want a padding corresponding to the entire forged message,\n",
    "not just the extra bytes we are feeding the cloned SHA-1 with.\n",
    "\n",
    "Second problem with padding:\n",
    "The state we just cloned does not correspond to just `key + original_msg`\n",
    "but to this plus the padding that was added by SHA-1.\n",
    "This is our X: `key + original_msg + glue_padding`\n",
    "\n",
    "We need to know the value of this \"glue padding\"\n",
    "because we will have to integrate it in our forged message.\n",
    "\n",
    "Finding the padding value is very simple ...\n",
    "when you know the length of the input you are padding.\n",
    "We know the length of the original message;\n",
    "but are we supposed to know the lenght of the secret key?\n",
    "From the instructions, it seems that we don't:\n",
    "\n",
    "> Note that to generate the glue padding,\n",
    "> you'll need to know the original bit length of the message;\n",
    "> the message itself is known to the attacker,\n",
    "> but the secret key isn't, so you'll need to guess at it.\n",
    "\n",
    "This seems a bit weird;\n",
    "In many cases even if the attacker does not know *the value* of the key,\n",
    "he should be able to know its length,\n",
    "for keysizes are rarely random and are often set by a strict standard.\n",
    "\n",
    "Suppose we don't know the key size, what can we do ?\n",
    "We could just try to guess (which is what the instructions seem to indicate)\n",
    "and have several candidate message-MAC pairs, one for each keysize candidate.\n",
    "You can just submit all of them to the victim\n",
    "and hope that doing so is not flagged as \"suspicious behavior\".\n",
    "Actually even if the victim server is using behavioral detection,\n",
    "this behavior is not that likely to trigger a warning\n",
    "because we don't have that many candidates to try out:\n",
    "we are trying different *key sizes* not *key values*\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### New functions and MAC forgery\n",
    "\n",
    "We are going to need sha1 padding as a separate function,\n",
    "and with a parameter to modify its behaviour:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sha1_padding(msg, forced_msg_byte_length=None):\n",
    "\n",
    "    # we are limiting ourselves to messages being byte strings\n",
    "    # even though specification mentions bit strings of any length\n",
    "    assert isinstance(msg, bytes)\n",
    "\n",
    "    # message length in bits\n",
    "    if forced_msg_byte_length == None:\n",
    "        msg_length = len(msg)*8\n",
    "    else:\n",
    "        msg_length = forced_msg_byte_length*8\n",
    "\n",
    "    # we must append a \"1\" bit.\n",
    "    # since we are always working with bytes\n",
    "    # the appended bit will always be at the beginning of the next byte\n",
    "\n",
    "    # computing the number of \"zeroes\" to append\n",
    "    # we need msg_length + 1 + m + 64 = 0 mod 512\n",
    "    # thus m = -(msg_length + 1 + 64) mod 512\n",
    "    m = -(msg_length + 1 + 64) % 512\n",
    "\n",
    "    # m+1 will always be a multiple of 8 in our case\n",
    "    padded_msg = (msg\n",
    "                  + bytes([0b10000000])\n",
    "                  + b'\\x00'*(m//8)\n",
    "                  + msg_length.to_bytes(8, byteorder='big')\n",
    "                 )\n",
    "\n",
    "    return padded_msg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here is our new SHA-1 function\n",
    "with optional parameters to allow cloning:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sha1(msg, state=None, message_added_length=None):\n",
    "    # following RFC 3174\n",
    "    # https://tools.ietf.org/html/rfc3174\n",
    "\n",
    "    # we are prioritizing readability and similarity with the specs\n",
    "    # over optimization\n",
    "\n",
    "    # we are always in big-endian form in SHA1\n",
    "    # (Section 2.c: \"The least significant four bits of the integer are\n",
    "    # represented by the right-most hex digit of the word representation\")\n",
    "\n",
    "    # to use as a bit mask for reduction modulo 2^32\n",
    "    MAX_WORD = 0xFFFFFFFF\n",
    "\n",
    "    # Section 3: Operations on Words\n",
    "\n",
    "    def S(X, n):\n",
    "        'circular left shift (a.k.a \"rotate left\")'\n",
    "        # don't forget reduction modulo 2^32 !\n",
    "        # it is not explicitely written in the formula in the RFC\n",
    "        # (it is in the prose below it though)\n",
    "        return ((X << n) | (X >> (32-n))) & MAX_WORD\n",
    "\n",
    "    # Section 4: Padding\n",
    "    \n",
    "    if message_added_length == None:\n",
    "        padded_msg = sha1_padding(msg)\n",
    "    else:\n",
    "        forced_msg_byte_length = len(msg) + message_added_length\n",
    "        padded_msg = sha1_padding(msg, forced_msg_byte_length)\n",
    "\n",
    "    words = [int.from_bytes(w, byteorder='big')\n",
    "             for w in split_bytes_in_blocks(padded_msg, 4)]\n",
    "\n",
    "    # \"The padded message will contain 16 * n words\"\n",
    "    n = len(words)/16\n",
    "    assert n.is_integer()\n",
    "    n = int(n)\n",
    "\n",
    "    # \"The padded message is regarded as a sequence of n blocks M(1), M(2), …\"\n",
    "    M = split_bytes_in_blocks(words, 16)\n",
    "\n",
    "    # Section 5: Functions and Constants Used\n",
    "\n",
    "    def f(t, B, C, D):\n",
    "        if 0 <= t <= 19:\n",
    "            return (B & C) | ((~B) & D)\n",
    "        elif 20 <= t <= 39 or 60 <= t <= 79:\n",
    "            return B ^ C ^ D\n",
    "        elif 40 <= t <= 59:\n",
    "            return (B & C) | (B & D) | (C & D)\n",
    "        else:\n",
    "            raise Exception('t must be between 0 and 79 inclusive')\n",
    "\n",
    "    # this could be optimized, for instance with an array\n",
    "    # but this way is closer to how it is described in the specs\n",
    "    def K(t):\n",
    "        if 0 <= t <= 19:\n",
    "            return 0x5A827999\n",
    "        elif 20 <= t <= 39:\n",
    "            return 0x6ED9EBA1\n",
    "        elif 40 <= t <= 59:\n",
    "            return 0x8F1BBCDC\n",
    "        elif 60 <= t <= 79:\n",
    "            return 0xCA62C1D6\n",
    "        else:\n",
    "            raise Exception('t must be between 0 and 79 inclusive')\n",
    "\n",
    "    # Section 6: Computing the Message Digest\n",
    "    # Using \"method 1\" (Section 6.1)\n",
    "\n",
    "    if state == None:\n",
    "        H0 = 0x67452301\n",
    "        H1 = 0xEFCDAB89\n",
    "        H2 = 0x98BADCFE\n",
    "        H3 = 0x10325476\n",
    "        H4 = 0xC3D2E1F0\n",
    "    else:\n",
    "        # SHA-1 state cloning\n",
    "        assert isinstance(state, tuple)\n",
    "        assert len(state) == 5\n",
    "        assert all(isinstance(x, int) for x in state)\n",
    "\n",
    "        H0, H1, H2, H3, H4 = state\n",
    "\n",
    "    for i in range(len(M)):\n",
    "        W = M[i]\n",
    "        assert len(W) == 16\n",
    "        \n",
    "        for t in range(16, 80):\n",
    "            W.append( S(W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16],\n",
    "                        n=1) )\n",
    "\n",
    "        A, B, C, D, E = H0, H1, H2, H3, H4\n",
    "\n",
    "        for t in range(80):\n",
    "            TEMP = (S(A, 5) + f(t, B, C, D) + E + W[t] + K(t)) & MAX_WORD\n",
    "\n",
    "            E = D; D = C; C = S(B, 30); B = A; A = TEMP\n",
    "\n",
    "        H0 = (H0 + A) & MAX_WORD\n",
    "        H1 = (H1 + B) & MAX_WORD\n",
    "        H2 = (H2 + C) & MAX_WORD\n",
    "        H3 = (H3 + D) & MAX_WORD\n",
    "        H4 = (H4 + E) & MAX_WORD\n",
    "\n",
    "    result = b''.join(H.to_bytes(4, byteorder='big') for H in [H0, H1, H2, H3, H4])\n",
    "\n",
    "    return result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Okay, now we're ready"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div style=\"border:1px solid #c3e6cb;padding:.75rem 3rem;border-radius:.5rem;font-weight:bold;text-align: center;background-color:#d4edda;color:#155724;border-color:#c3e6cb;\">OK</div>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# say that's our \"guess\"\n",
    "keysize = 16\n",
    "\n",
    "# to get the glue padding\n",
    "# we will compute a padding of a random key of the proper size\n",
    "# and the original message,\n",
    "# and we only keep the added bytes\n",
    "glue_padding = sha1_padding(os.urandom(keysize) + original_msg)[keysize + len(original_msg):]\n",
    "\n",
    "message_added_length = keysize + len(original_msg) + len(glue_padding)\n",
    "extra_text = b';admin=true'\n",
    "forged_mac = sha1(extra_text, (H0, H1, H2, H3, H4), message_added_length)\n",
    "\n",
    "forged_msg = original_msg + glue_padding + extra_text\n",
    "\n",
    "html_test(sha1(key + forged_msg) == forged_mac)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
