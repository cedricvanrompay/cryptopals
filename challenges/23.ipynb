{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Challenge 23: Clone an MT19937 RNG from its output\n",
    "\n",
    "> The internal state of MT19937 consists of 624 32 bit integers.\n",
    "> \n",
    "> For each batch of 624 outputs, MT permutes that internal state. By permuting state regularly, MT19937 achieves a period of 2**19937, which is Big.\n",
    "> \n",
    "> Each time MT19937 is tapped, an element of its internal state is subjected to a tempering function that diffuses bits through the result.\n",
    "> \n",
    "> The tempering function is invertible; you can write an \"untemper\" function that takes an MT19937 output and transforms it back into the corresponding element of the MT19937 state array.\n",
    "> \n",
    "> To invert the temper transform, apply the inverse of each of the operations in the temper transform in reverse order. There are two kinds of operations in the temper transform each applied twice; one is an XOR against a right-shifted value, and the other is an XOR against a left-shifted value AND'd with a magic number. So you'll need code to invert the \"right\" and the \"left\" operation.\n",
    "> \n",
    "> Once you have \"untemper\" working, create a new MT19937 generator, tap it for 624 outputs, untemper each of them to recreate the state of the generator, and splice that state into a new instance of the MT19937 generator.\n",
    "> \n",
    "> The new \"spliced\" generator should predict the values of the original.\n",
    "> \n",
    "> **Stop and think for a second:** How would you modify MT19937 to make this attack hard? What would happen if you subjected each tempered output to a cryptographic hash?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is the tempering function from MT19937:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def MT_temper(x):\n",
    "    (w, n, m, r) = (32, 624, 397, 31)\n",
    "    a = 0x9908B0DF\n",
    "    (u, d) = (11, 0xFFFFFFFF)\n",
    "    (s, b) = (7, 0x9D2C5680)\n",
    "    (t, c) = (15, 0xEFC60000)\n",
    "    l = 18\n",
    "    f = 1812433253\n",
    "    \n",
    "    y = x ^ ((x >> u) & d)\n",
    "    y = y ^ ((y << s) & b)\n",
    "    y = y ^ ((y << t) & c)\n",
    "    return y ^ (y >> l)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see it's 4 operations that are more or less similar:\n",
    "the value is shifted and a mask is applied, then the result is XORed against the previous value.\n",
    "\n",
    "The instructions tell us these operations are invertible,\n",
    "so let's have a look.\n",
    "\n",
    "Let $ y = x \\oplus \\left( (x \\ll s) \\wedge m \\right) $ where $ s $ is the shift amount, $ m $ the mask,\n",
    "$\\ll$ is bit shifting operator and $\\wedge$ if the “bitwise and” operator.\n",
    "\n",
    "Let's try to find $ x $ from $ y $ and known parameters $m$ and $s$.\n",
    "\n",
    "We will represents bits of numbers as $ y = Y_0 ~ Y_1 ~ \\dots ~ Y_{w-1} $ or `Y0 Y1 ... Yw-1`\n",
    "\n",
    "The operation to invert can then be represented as\n",
    "\n",
    "      X2 X3 X4 X5 0  0 \n",
    "    & M0 M1 M2 M3 M4 M5\n",
    "    ⊕ X0 X1 X2 X3 X4 X5\n",
    "    = Y0 Y1 Y2 Y3 Y4 Y5\n",
    "    \n",
    "(In the example, shifting value is 2 and direction is left)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This can be written in a more mathematical way as:\n",
    "\n",
    "$$ Y_n = \n",
    "\\begin{cases}\n",
    "    X_n \\oplus ( X_{n + ds} \\wedge M_n ) & \\text{ if } 0 \\leq n+ds \\leq w-1\\\\\n",
    "    X_n                                & \\text{ otherwise}\n",
    "\\end{cases}\n",
    "$$\n",
    "\n",
    "Where $ d \\in \\{-1, 1\\} $ represents the shift direction (left or right)\n",
    "and $w$ is the bit length we are working with.\n",
    "Recall that $M_n \\wedge 0 = 0$ whatever the value of $M_n$ is,\n",
    "and this is why our second case is so trivial.\n",
    "\n",
    "To invert we can then just reverse the previous expression and have:\n",
    "\n",
    "$$ X_n = \n",
    "\\begin{cases}\n",
    "    Y_n \\oplus ( X_{n + ds} \\wedge M_n ) & \\text{ if } 0 \\leq n+ds \\leq w-1\\\\\n",
    "    Y_n                                & \\text{ otherwise}\n",
    "\\end{cases}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The strategy is then to recover $ x $ bit-by-bit:\n",
    "we start with the ones that are trivial to recover\n",
    "because shifting makes sure that they did not change from $ x $ to $ y $\n",
    "(bits `X4` and `X5` in our example).\n",
    "\n",
    "We are then able to recover the other bytes (`X2` and `X3` in our example)\n",
    "because their value can be computed from the formula and the value\n",
    "of the bits we just recovered, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I tried to find a more “elegant” way to do this, i.e. not bit-by-bit\n",
    "but instead by combining $ y $ and $ m $.\n",
    "I managed to find how you do it but it's not at all more “elegant”.\n",
    "\n",
    "Looking at other solutions everyone seems to use Python's bitwise operations on integers\n",
    "and this leads to solutions that I think are not very elegant.\n",
    "Several things I wanted to improve:\n",
    "- have function work on every set of parameters (insead of relying on hardcoded constants)\n",
    "- avoiding having one function to invert left shift and another one for right shift\n",
    "\n",
    "So I used the following strategy:\n",
    "numbers are converted to lists of bits first\n",
    "because lists are so much easier to work with\n",
    "(especially if you are trying to access bits separately).\n",
    "\n",
    "Note how much the core logic\n",
    "(with `if n < shift:`)\n",
    "ressembles the math formula above!\n",
    "The power of lists makes it so much easier to reason about individual bits."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div style=\"border:1px solid #c3e6cb;padding:.75rem 3rem;border-radius:.5rem;font-weight:bold;text-align: center;background-color:#d4edda;color:#155724;border-color:#c3e6cb;\">OK</div>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# using a function that declares internal functions every time\n",
    "# is not the most elegant thing to do,\n",
    "# but it just felt wrong to create an object\n",
    "# for something (untempering) that does not require any state\n",
    "def untemper(y):\n",
    "    (w, n, m, r) = (32, 624, 397, 31)\n",
    "    a = 0x9908B0DF\n",
    "    (u, d) = (11, 0xFFFFFFFF)\n",
    "    (s, b) = (7, 0x9D2C5680)\n",
    "    (t, c) = (15, 0xEFC60000)\n",
    "    l = 18\n",
    "    f = 1812433253\n",
    "\n",
    "    def int_to_bit_list(x):\n",
    "        return [int(b) for b in '{:032b}'.format(x)]\n",
    "\n",
    "    def bit_list_to_int(l):\n",
    "        return int(''.join(str(x) for x in l), base=2)\n",
    "\n",
    "    def invert_shift_mask_xor(y, direction, shift, mask=0xFFFFFFFF):\n",
    "        y = int_to_bit_list(y)\n",
    "        mask = int_to_bit_list(mask)\n",
    "\n",
    "        if direction == 'left':\n",
    "            y.reverse()\n",
    "            mask.reverse()\n",
    "        else:\n",
    "            assert direction == 'right'\n",
    "\n",
    "        x = [None]*32\n",
    "        for n in range(32):\n",
    "            if n < shift:\n",
    "                x[n] = y[n]\n",
    "            else:\n",
    "                x[n] = y[n] ^ (mask[n] & x[n-shift])\n",
    "\n",
    "        if direction == 'left':\n",
    "            x.reverse()\n",
    "\n",
    "        return bit_list_to_int(x)\n",
    "\n",
    "    xx = y\n",
    "    xx = invert_shift_mask_xor(xx, direction='right', shift=l)\n",
    "    xx = invert_shift_mask_xor(xx, direction='left', shift=t, mask=c)\n",
    "    xx = invert_shift_mask_xor(xx, direction='left', shift=s, mask=b)\n",
    "    xx = invert_shift_mask_xor(xx, direction='right', shift=u, mask=d)\n",
    "\n",
    "    return xx\n",
    "\n",
    "# testing\n",
    "from random import randint\n",
    "from libmatasano import html_test\n",
    "for _ in range(10):\n",
    "    x = randint(0, 0xFFFFFFF)\n",
    "    y = MT_temper(x)\n",
    "    assert untemper(y) == x\n",
    "    \n",
    "html_test(True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div style=\"border:1px solid #c3e6cb;padding:.75rem 3rem;border-radius:.5rem;font-weight:bold;text-align: center;background-color:#d4edda;color:#155724;border-color:#c3e6cb;\">OK</div>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# cloning time!\n",
    "from libmatasano import MT19937_32\n",
    "prng = MT19937_32()\n",
    "\n",
    "state = [untemper(next(prng)) for _ in range(624)] \n",
    "\n",
    "cloned_prng = MT19937_32(state=state)\n",
    "\n",
    "for _ in range(20):\n",
    "    assert next(prng) == next(cloned_prng)\n",
    "\n",
    "html_test(True)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
