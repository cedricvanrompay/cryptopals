{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Challenge 24: Create the MT19937 stream cipher and break it\n",
    "\n",
    "> You can create a trivial stream cipher out of any PRNG; use it to generate a sequence of 8 bit outputs and call those outputs a keystream. XOR each byte of plaintext with each successive byte of keystream.\n",
    "> \n",
    "> Write the function that does this for MT19937 using a 16-bit seed. Verify that you can encrypt and decrypt properly. This code should look similar to your CTR code.\n",
    "> \n",
    "> Use your function to encrypt a known plaintext (say, 14 consecutive 'A' characters) prefixed by a random number of random characters.\n",
    "> \n",
    "> From the ciphertext, recover the \"key\" (the 16 bit seed).\n",
    "> \n",
    "> Use the same idea to generate a random \"password reset token\" using MT19937 seeded from the current time.\n",
    "> \n",
    "> Write a function to check if any given password token is actually the product of an MT19937 PRNG seeded with the current time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import log2\n",
    "\n",
    "from libmatasano import MT19937_32"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "MT19937_32 outputs 32-bits numbers, and we need bytes (4 bits) to operate on.\n",
    "So we're going to split every 32-bit number into 4 bytes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mt19937_keystream_generator(key):\n",
    "    assert log2(key) <= 16\n",
    "    prng = MT19937_32(key)\n",
    "    while True:\n",
    "        rand_nb = next(prng)\n",
    "        # 32 bits = 4 bytes\n",
    "        yield from rand_nb.to_bytes(4, byteorder='big')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def transform_mt19937(msg, key):\n",
    "    assert isinstance(key, int)\n",
    "    keystream = mt19937_keystream_generator(key)\n",
    "    \n",
    "    return bytes([ x^y for (x,y) in zip(msg, keystream)])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'Yb\\x07C\\x10F\\x96\\xbb\\xfa0\\xe0\\x07'"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "key = 1234\n",
    "ctxt = transform_mt19937(b'hello there!', key)\n",
    "ctxt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'hello there!'"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "transform_mt19937(ctxt, key)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Breaking the Cipher\n",
    "\n",
    "When I first saw that we had to \"crack the MT19937 stream cipher\",\n",
    "I thought we were going to re-use the previous attack:\n",
    "encrypting known plaintext gives you access to the corresponding bytes of the keystream,\n",
    "and with enough keystream bytes\n",
    "you can clone the state with the technique from the previous challenge.\n",
    "Then, you are able to re-generate the keystream for all bytes after the known plaintext,\n",
    "so you can decrypt whatever was appended to the known plaintext.\n",
    "\n",
    "Turns out, that's not what we are supposed to do here:\n",
    "there is no plaintext *appended* to our chosen plaintext\n",
    "(here it's *prepended*),\n",
    "and they ask us to recover the key.\n",
    "Also I was surprised to see that they suggest to generate\n",
    "just *14 bytes* of known plaintext! That's way too little to do state cloning.\n",
    "\n",
    "I was thus trying to see if even with a part of the state\n",
    "I could re-wind the recurrence relation *and* the initialization procedure\n",
    "up to the seed.\n",
    "And it wasn't easy...\n",
    "\n",
    "But! It's actually far more simple than that:\n",
    "**brute force**.\n",
    "The number of possible keys is super small so you can just try them all.\n",
    "They even *insist* on it in the instructions: the cipher is using a *16-bits seed*.\n",
    "It's kind of weird actually because from the specifications of MT19937\n",
    "the seed seems to be 32 bits.\n",
    "Well even 32 bits should be small enough to crack,\n",
    "it would just take longer.\n",
    "\n",
    "It's a bit disapointing to use brute force this far in the challenges..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div style=\"border:1px solid #c3e6cb;padding:.75rem 3rem;border-radius:.5rem;font-weight:bold;text-align: center;background-color:#d4edda;color:#155724;border-color:#c3e6cb;\">OK</div>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import os\n",
    "from random import randint\n",
    "\n",
    "from libmatasano import html_test\n",
    "\n",
    "MAX_SEED = (1<<16) - 1\n",
    "\n",
    "key = randint(1, MAX_SEED)\n",
    "\n",
    "prefix_size = randint(2,20)\n",
    "prefix = os.urandom(prefix_size)\n",
    "\n",
    "ctxt = transform_mt19937(prefix + b'A'*14, key)\n",
    "\n",
    "for i in range(1, MAX_SEED):\n",
    "    p = transform_mt19937(ctxt, i)\n",
    "    if p.endswith(b'A'*14):\n",
    "        result = i\n",
    "        break\n",
    "else:\n",
    "    html_test(False)\n",
    "    \n",
    "html_test(result==key)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Breaking the \"password reset token\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gen_token():\n",
    "    seed = int(time()) & MAX_SEED\n",
    "    pseudo_random_byte_gen = mt19937_keystream_generator(seed)\n",
    "    token = bytes(next(pseudo_random_byte_gen) for _ in range(16))\n",
    "    return token"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'[\\xd95(\\xb3EO\\x06^<3/\\xfc\\xe7\\xa9\\x1f'"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "token = gen_token()\n",
    "token"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "def is_generated_with_mt19937(token):\n",
    "    for i in range(1, MAX_SEED):\n",
    "        pseudo_random_byte_gen = mt19937_keystream_generator(i)\n",
    "        guess = bytes(next(pseudo_random_byte_gen) for _ in range(16))\n",
    "        if guess == token:\n",
    "            return True\n",
    "    else:\n",
    "        return False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div style=\"border:1px solid #c3e6cb;padding:.75rem 3rem;border-radius:.5rem;font-weight:bold;text-align: center;background-color:#d4edda;color:#155724;border-color:#c3e6cb;\">OK</div>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "assert is_generated_with_mt19937(token)\n",
    "assert not is_generated_with_mt19937(os.urandom(16))\n",
    "html_test(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A bit boring. Or is there something I'm missing ?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
