{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Challenge 21: Implement the MT19937 Mersenne Twister RNG\n",
    "> \n",
    "> You can get the psuedocode for this from Wikipedia.\n",
    "> \n",
    "> If you're writing in Python, Ruby, or (gah) PHP, your language is probably already giving you MT19937 as \"rand()\"; don't use rand(). Write the RNG yourself."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is the Wikipedia article (see Section “Algorithmic detail”):\n",
    "\n",
    "https://en.m.wikipedia.org/wiki/Mersenne_Twister\n",
    " \n",
    "The description may be a bit impressive,\n",
    "but note that first we don't have to pick parameters $ w, m, n, r \\dots$,\n",
    "they are already fixed,\n",
    "and second we don't have to actually construct the A and T matrices:\n",
    "applying these matrices is equivalent to simply applying a few bitwise operations\n",
    "that are explicitely given in the Wikipedia article.\n",
    "\n",
    "After removing all this complexity,\n",
    "this is what we are left with:\n",
    "\n",
    "- MT19937-32 keeps a state\n",
    "  in the form of an array of $ n $ 32-bits integers\n",
    "  that must be initialized before we output the first pseudo-random number.\n",
    "- Initializing the state is done as follow:\n",
    "  the first value is the seed,\n",
    "  then each value is computed from the previous value in the state\n",
    "  using some recurrence formula involving XOR, right-shift and integer multiplication\n",
    "  (with the output of the multiplication being truncated to fit into 32 bits)\n",
    "- then, we can finally ouput our first pseudo-random number:\n",
    "  we compute a value noted $ x $ that is computed from the first value in the state\n",
    "  (at first it's the seed, though),\n",
    "  the second value in the state,\n",
    "  and third value somewhere in the state\n",
    "  (which one exactly is controlled by the $ m $ parameters, set to 397 apparently)\n",
    "- this $ x $ value *is not the number that we output*.\n",
    "  before outputing it we apply what they call a *tempering transform*\n",
    "  (the T matrix, that is some more bitwise operations).\n",
    "  It is the output of the tempering transform that we output.\n",
    "- the value $ x $ is the one that is inserted at the end of the state,\n",
    "  while the first value of the state is popped out.\n",
    "  this way, the next $ x $ value will be computed out of the different values\n",
    "  (the former second, the former third and the former $(m+1)$-nth value of the state)\n",
    "  \n",
    "Example code on Wikipedia and some other write-ups\n",
    "use what seems to be some sort of \"optimization\":\n",
    "they precompute the $ n $ first \"$ x $\" values right after initialization\n",
    "(or when the generator is asked for a first output value).\n",
    "\n",
    "It may be interesting for performance\n",
    "(I'm not even sure it makes a big difference)\n",
    "but I am more focused on readability and elegance here,\n",
    "so instead I chose to generate a new $ x $ only when needed,\n",
    "that is, at each call.\n",
    "\n",
    "Also this is the perfect opportunity to use the `yield` keyword in Python\n",
    "that allows to easily create \"generators\",\n",
    "i.e. objects that can be called several times\n",
    "and keep a state between two calls.\n",
    "For more information about generators and the `yield` keyword,\n",
    "see the [Python Documentation][1] and the [related PEP][2]\n",
    "\n",
    "[1]: https://docs.python.org/3/reference/expressions.html#yield-expressions\n",
    "[2]: https://www.python.org/dev/peps/pep-0255/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I generated some test data\n",
    "using the MT19937 implementation provided with C++\n",
    "because Python does not seems to provide access to the numbers outputed by it directly.\n",
    "\n",
    "Actually I used the code from here:\n",
    "\n",
    "https://www.guyrutenberg.com/2014/05/03/c-mt19937-example/\n",
    "\n",
    "Put a for loop in it,\n",
    "and executed it in the online C++ shell:\n",
    "\n",
    "http://www.cpp.sh/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from libmatasano import html_test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def MT19937_32(seed=5489):\n",
    "    '''Mersenne-Twister PRNG, 32-bit version'''\n",
    "    # parameters for MT19937-32\n",
    "    (w, n, m, r) = (32, 624, 397, 31)\n",
    "    a = 0x9908B0DF\n",
    "    (u, d) = (11, 0xFFFFFFFF)\n",
    "    (s, b) = (7, 0x9D2C5680)\n",
    "    (t, c) = (15, 0xEFC60000)\n",
    "    l = 18\n",
    "    f = 1812433253\n",
    "\n",
    "    # masks (to apply with an '&' operator)\n",
    "    # ---------------------------------------\n",
    "    # zeroes out all bits except \"the w-r highest bits\"\n",
    "    # (i.e. with our parameters the single highest bit, since w-r=1)\n",
    "    high_mask = ((1<<w) - 1) - ((1<<r) - 1)\n",
    "    # zeroes out all bits excepts \"the r lowest bits\"\n",
    "    low_mask = (1<<r)-1\n",
    "\n",
    "    def twist(x):\n",
    "        return (x >> 1)^a if (x % 2 == 1) else x >> 1\n",
    "\n",
    "    # initialization (populating the state)\n",
    "    state = list()\n",
    "    state.append(seed)\n",
    "    for i in range(1, n):\n",
    "        prev = state[-1]\n",
    "        # the \"& d\" is to take only the lowest 32 bits of the result\n",
    "        x = (f * (prev ^ (prev >> (w-2))) + i) & d\n",
    "        state.append(x)\n",
    "\n",
    "    while True:\n",
    "        x = state[m] ^ twist((state[0] & high_mask) + (state[1] & low_mask))\n",
    "\n",
    "        # tempering transform and output\n",
    "        y = x ^ ((x >> u) & d)\n",
    "        y = y ^ ((y << s) & b)\n",
    "        y = y ^ ((y << t) & c)\n",
    "        yield y ^ (y >> l)\n",
    "\n",
    "        # note that it's the 'x' value\n",
    "        # that we insert in the state\n",
    "        state.pop(0)\n",
    "        state.append(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div style=\"border:1px solid #c3e6cb;padding:.75rem 3rem;border-radius:.5rem;font-weight:bold;text-align: center;background-color:#d4edda;color:#155724;border-color:#c3e6cb;\">OK</div>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import json\n",
    "\n",
    "with open('data/21.json') as f:\n",
    "    test_data = json.load(f)\n",
    "\n",
    "for key in test_data:\n",
    "    seed = int(key)\n",
    "    assert all(x==y for (x,y) in zip(MT19937_32(seed), test_data[key]))\n",
    "    \n",
    "html_test(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tadaa !\n",
    "\n",
    "I had to debug my code a little bit though\n",
    "because at first I did a bad job at\n",
    "\"selecting the w-r highest bits of one and the r lowets bits of another\".\n",
    "It was nice to get some practice at bitwise operations.\n",
    "\n",
    "I was lucky I could take the Python implementation of someone else\n",
    "(https://github.com/akalin/cryptopals-python3)\n",
    "and run it with the [Python debugger][pdb]\n",
    "to see exactly where our values where diverging and spot my mistakes.\n",
    "This is the reason why in some specifications they give all the intermediate values for an example value, for instance in the [AES specs][] (See Appendix C – Example Vectors).\n",
    "\n",
    "[pdb]: https://docs.python.org/3/library/pdb.html\n",
    "[AES specs]: https://doi.org/10.6028/NIST.FIPS.197"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
