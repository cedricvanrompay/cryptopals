{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Set 4: Stream crypto and randomness\n",
    "\n",
    "## Challenge 25: Break \"random access read/write\" AES CTR\n",
    "\n",
    "> Back to CTR. Encrypt the recovered plaintext from [this file](data/25.txt) (the ECB exercise) under CTR with a random key (for this exercise the key should be unknown to you, but hold on to it).\n",
    "> \n",
    "> Now, write the code that allows you to \"seek\" into the ciphertext, decrypt, and re-encrypt with different plaintext. Expose this as a function, like, `edit(ciphertext, key, offset, newtext)`.\n",
    "> \n",
    "> Imagine the \"edit\" function was exposed to attackers by means of an API call that didn't reveal the key or the original plaintext; the attacker has the ciphertext and controls the offset and \"new text\".\n",
    "> \n",
    "> Recover the original plaintext.\n",
    "> \n",
    "> **Food for thought:** A folkloric supposed benefit of CTR mode is the ability to easily \"seek forward\" into the ciphertext; to access byte N of the ciphertext, all you need to be able to do is generate byte N of the keystream. Imagine if you'd relied on that advice to, say, encrypt a disk.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from base64 import b64decode\n",
    "from itertools import islice\n",
    "import os\n",
    "\n",
    "from libmatasano import (\n",
    "    bxor,\n",
    "    transform_aes_128_ctr,\n",
    "    aes_128_ctr_keystream_generator,\n",
    "    html_test\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First let's get the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('data/25.txt') as f:\n",
    "    data = b64decode(f.read())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I'm wondering if there's something I am doing wrong because\n",
    "the \"data\" looks meaningless while the instructions say that it is supposed to be\n",
    "\"recovered plaintext\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'\\t\\x120\\xaa\\xde>\\xb30\\xdb\\xaaCX\\xf8\\x8d*l7\\xb7-\\x0c\\xf4\\xc2,4J\\xecAB\\xd0\\x0c'"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data[:30]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Well it doesn't really matter for what we're doing here,\n",
    "as long as we are able to recover this data from the ciphertext."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We encrypt the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "key = os.urandom(16)\n",
    "nonce = 0\n",
    "\n",
    "ctxt = transform_aes_128_ctr(data, key, nonce)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is the \"edit\" function.\n",
    "We compute the modified chunk of ciphertext\n",
    "by encrypting the new text,\n",
    "meaning by XORing the new text with the keystream at the proper index.\n",
    "\n",
    "Then we append and prepend the parts of the ciphertext that do not have to change."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def edit(ctxt, key, nonce, offset, newtext):\n",
    "    keystream = aes_128_ctr_keystream_generator(key, nonce)\n",
    "\n",
    "    new_chunk = bxor(newtext,\n",
    "                     islice(keystream, offset, offset+len(newtext)))\n",
    "\n",
    "    result = ctxt[:offset] + new_chunk + ctxt[offset+len(newtext):]\n",
    "    return result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Quick test:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "b'\\t\\x120\\xaa\\xde>\\xb30\\xdb\\xaaCX\\xf8\\x8d*l7\\xb7-\\x0c'\n",
      "b'\\t\\x120\\xaa\\xde>\\xb30\\xdb\\xaaLOOOOOL\\xb7-\\x0c'\n"
     ]
    }
   ],
   "source": [
    "edited_ctxt = edit(ctxt, key, nonce,\n",
    "                  offset=10, newtext=b'LOOOOOL')\n",
    "\n",
    "print(data[:20])\n",
    "print(transform_aes_128_ctr(edited_ctxt, key, nonce)[:20])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Attack\n",
    "\n",
    "This \"edit\" function allows us to get two different messages\n",
    "XORed against the same keystream.\n",
    "We already saw that this is bad (see challenges 19 and 20):\n",
    "\n",
    "$$ ( Msg_1 \\oplus Key ) \\oplus ( Msg_2 \\oplus Key ) = Msg_1 \\oplus Msg_2 $$\n",
    "\n",
    "Then if you know $ Msg_2 $ you can recover $ Msg_1 $.\n",
    "\n",
    "But here it's even easier: we can *choose* the second message.\n",
    "Then we can just ask to encrypt a \"message\" being only null bytes,\n",
    "and... the ciphertext is equal to the keystream, which we can use to decrypt.\n",
    "\n",
    "It's that simple."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "recovered_keystream = edit(ctxt, key, nonce,\n",
    "                           offset=0,\n",
    "                           newtext=b'\\x00'*len(ctxt))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "recovered_plaintext = bxor(ctxt, recovered_keystream)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div style=\"border:1px solid #c3e6cb;padding:.75rem 3rem;border-radius:.5rem;font-weight:bold;text-align: center;background-color:#d4edda;color:#155724;border-color:#c3e6cb;\">OK</div>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "html_test(recovered_plaintext == data)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
