{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2b533b64",
   "metadata": {},
   "source": [
    "# Challenge 31: Implement and break HMAC-SHA1 with an artificial timing leak\n",
    "\n",
    "*https://cryptopals.com/sets/4/challenges/31*\n",
    "\n",
    "> The psuedocode on Wikipedia should be enough. HMAC is very easy.\n",
    "> \n",
    "> Using the web framework of your choosing (Sinatra, web.py, whatever), write a tiny application that has a URL that takes a \"file\" argument and a \"signature\" argument, like so:\n",
    "> \n",
    ">     http://localhost:9000/test?file=foo&signature=46b4ec586117154dacd49d664e5d63fdc88efb51\n",
    "> \n",
    "> Have the server generate an HMAC key, and then verify that the \"signature\" on incoming requests is valid for \"file\", using the \"==\" operator to compare the valid MAC for a file with the \"signature\" parameter (in other words, verify the HMAC the way any normal programmer would verify it).\n",
    "> \n",
    "> Write a function, call it \"insecure_compare\", that implements the == operation by doing byte-at-a-time comparisons with early exit (ie, return false at the first non-matching byte).\n",
    "> \n",
    "> In the loop for \"insecure_compare\", add a 50ms sleep (sleep 50ms after each byte).\n",
    "> \n",
    "> Use your \"insecure_compare\" function to verify the HMACs on incoming requests, and test that the whole contraption works. Return a 500 if the MAC is invalid, and a 200 if it's OK.\n",
    "> \n",
    "> Using the timing leak in this application, write a program that discovers the valid MAC for any file. \n",
    "> \n",
    "> ### Why artificial delays?\n",
    "> \n",
    "> Early-exit string compares are probably the most common source of cryptographic timing leaks, but they aren't especially easy to exploit. In fact, many timing leaks (for instance, any in C, C++, Ruby, or Python) probably aren't exploitable over a wide-area network at all. To play with attacking real-world timing leaks, you have to start writing low-level timing code. We're keeping things cryptographic in these challenges."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d196a060",
   "metadata": {},
   "source": [
    "HMAC can be thought of *“the proper way of implementing a MAC using a hash function”*. Its main interest is that it protects against length-extension attacks which we saw in the previous challenge. And, as the instructions say, it's *really* easy to implement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "d09cea14",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# https://docs.python.org/3/library/hashlib.html\n",
    "import hashlib\n",
    "\n",
    "from libmatasano import bxor\n",
    "\n",
    "def sha1(data):\n",
    "    '''single-call SHA-1\n",
    "    (instead of having several calls then a finalization call)'''\n",
    "    h = hashlib.sha1()\n",
    "    h.update(data)\n",
    "    return h.digest()\n",
    "\n",
    "def hmac_sha1(data, key):\n",
    "    # see https://datatracker.ietf.org/doc/html/rfc2104\n",
    "    IPAD = b'\\x36'*64\n",
    "    OPAD = b'\\x5C'*64\n",
    "\n",
    "    return sha1(\n",
    "        bxor(key, OPAD)\n",
    "        + sha1(\n",
    "            bxor(key, IPAD)\n",
    "            + data\n",
    "        )\n",
    "    )\n",
    "\n",
    "# Testing our implementation against a value I got using the \"cryptography\" library\n",
    "assert (\n",
    "    hmac_sha1(b'test message', b'test key')\n",
    "    == b'\\xbb?\\x1a\\xdc\\x11~\\xa0\\xed\\x15\\x9d\\x8ek\\xaa\\xfb\\x9d\\xff\\xe4\\x8caZ'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a33b9386",
   "metadata": {},
   "source": [
    "Now let's implement our “Web application”. I am not going to use a Web framework, this would really be overkill. It's not very realistic to upload a file only using a URL anyway, the file should be in the body of the request. Instead, we'll just have an object holding a MAC key and simulating the processing of a request."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "816dc933",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "from time import sleep\n",
    "from binascii import hexlify, unhexlify\n",
    "# https://docs.python.org/3/library/urllib.parse.html\n",
    "from urllib.parse import urlparse, parse_qs\n",
    "\n",
    "class Website:\n",
    "    def __init__(self):\n",
    "        self.mac_key = os.urandom(16)\n",
    "\n",
    "    def handle_query(self, url):\n",
    "        parsed_query = parse_qs(urlparse(url).query)\n",
    "\n",
    "        # note the \"[0]\": function `parse_qs` maps keys to *lists* of values\n",
    "        # because the HTTP protocol allows the same key to appear several time\n",
    "        # in a query string\n",
    "        file = parsed_query['file'][0]\n",
    "        signature = parsed_query['signature'][0]\n",
    "\n",
    "        sig_bytes = unhexlify(signature)\n",
    "\n",
    "        verify_signature(sig_bytes, file.encode(), self.mac_key)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5bfb2ae2",
   "metadata": {},
   "source": [
    "We have to create this `verify_signature` function. This is the core of this challenge."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "bebb6210",
   "metadata": {},
   "outputs": [],
   "source": [
    "class InvalidSignatureError(Exception):\n",
    "    pass\n",
    "\n",
    "def verify_signature(signature, data, key):\n",
    "    expected_signature = hmac_sha1(data, key)\n",
    "\n",
    "    for (sig_byte, expected_byte) in zip(signature, expected_signature):\n",
    "        if sig_byte != expected_byte:\n",
    "            # this is the \"early exit\":\n",
    "            # technically we can reject the signature\n",
    "            # *as soon as* we found one byte that differs.\n",
    "            # This however is what will cause the vulnerability\n",
    "            # that is exploited in this challenge.\n",
    "            raise InvalidSignatureError\n",
    "\n",
    "        # the \"artificial delay\" of 50 milliseconds we are asked to add\n",
    "        sleep(0.05)\n",
    "\n",
    "    # We don't return anything:\n",
    "    # if we didn't raise an exception it means the signature was valid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "df46a39f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# instanciation\n",
    "website = Website()\n",
    "\n",
    "# Some quick tests\n",
    "\n",
    "correct_signature = bytes.decode(hexlify(\n",
    "    hmac_sha1(str.encode('foo'), website.mac_key)\n",
    "))\n",
    "# should not raise an error\n",
    "website.handle_query(f\"http://localhost:9000/test?file=foo&signature={correct_signature}\")\n",
    "# should raise an error\n",
    "wrong_signature = correct_signature[:2] + 'fff' + correct_signature[5:]\n",
    "try:\n",
    "    website.handle_query(f\"http://localhost:9000/test?file=foo&signature={wrong_signature}\")\n",
    "    # unexpected\n",
    "    raise Exception('Expected an \"InvalidSignatureError\"')\n",
    "except InvalidSignatureError:\n",
    "    # expected\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56c0d7c7",
   "metadata": {},
   "source": [
    "Good, now let's break things.\n",
    "\n",
    "This challenge is about a timing attack: by observing the time it takes to process a query, the attacker can learn some information she is not supposed to get. Here, because function `verify_signature` exits as soon as it found a wrong byte, the time it takes to verify a signature will depend on the location of the first wrong byte. This allows the attacker to know how many bytes of the signature she sent are valid, and from this she can recover the correct signature byte-per-byte.\n",
    "\n",
    "It's probably better to just see it in action for the first byte: we are going to send 255 signatures for validation, one for each possible first byte. All of them will be rejected instantly because their first byte is wrong, except one request because the first byte is valid, so `verify_signature` will check the next byte instead of exiting immediately. Because we added a `sleep` instruction, the difference in time between verifying one byte and verifying two bytes is quite significant (50 ms), so it will be easy to notice.\n",
    "\n",
    "The first thing we need is a function that measure how long it takes to process a request:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "05d1d60e",
   "metadata": {},
   "outputs": [],
   "source": [
    "def measure_verification_time(signature, website):\n",
    "    start_time = time.perf_counter_ns()\n",
    "    try:\n",
    "        website.handle_query(f\"http://localhost:9000/test?file=foo&signature={signature}\")\n",
    "        raise Exception('signature was not rejected')\n",
    "    except InvalidSignatureError:\n",
    "        pass\n",
    "    end_time = time.perf_counter_ns()\n",
    "\n",
    "    duration = end_time - start_time\n",
    "    # we return duration in milliseconds\n",
    "    # (time.perf_counter_ns() returns nanoseconds)\n",
    "    return duration//1_000_000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "536ffca5",
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "\n",
    "timings_first_byte = [\n",
    "    # see https://docs.python.org/3/library/string.html#formatspec\n",
    "    # for more information on the formating mini-language (the \":02x\" thing)\n",
    "    (\n",
    "        measure_verification_time(f'{first_byte:02x}' + '0'*(15*2), website),\n",
    "        f'{first_byte:02x}',\n",
    "    )\n",
    "    for first_byte in range(256)\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c4318c8",
   "metadata": {},
   "source": [
    "Let's see for which value validation took the most time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "b32cef9b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[(50, '83'), (0, 'ff'), (0, 'fe'), (0, 'fd'), (0, 'fc')]"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sorted(timings_first_byte, reverse=True)[:5]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4931df21",
   "metadata": {},
   "source": [
    "Each value took less than 1 ms to validate, except one value where it took 50 ms. What do you think the first byte of the correct signature is?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "77f48e3e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'83'"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "correct_signature[:2]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8738313b",
   "metadata": {},
   "source": [
    "Bingo.\n",
    "\n",
    "So we found the first byte, now we send 255 more signatures, all of them having `6c` as their first byte, and having a different second byte. Again, the one that take the most time to validate is the correct guess for the second byte. By repeating this we can get the correct signature for our file even though we don't have the key."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "c8c56e83",
   "metadata": {},
   "outputs": [],
   "source": [
    "def recover_next_signature_byte(website, already_recovered):\n",
    "    timings = list()\n",
    "    for candidate_byte in range(256):\n",
    "        candidate_signature = (\n",
    "            already_recovered\n",
    "            + f'{candidate_byte:02x}'\n",
    "            + '0'*(16*2 - len(already_recovered) - 2)\n",
    "        )\n",
    "\n",
    "        duration = measure_verification_time(candidate_signature, website)\n",
    "\n",
    "        if timings:\n",
    "            # mean timing for *other* bytes\n",
    "            mean = sum(x[0] for x in timings) / len(timings)\n",
    "\n",
    "            if duration > mean + 30:\n",
    "                # this one took much longer,\n",
    "                # (at least 30 milseconds more than average)\n",
    "                # that's probably the correct byte,\n",
    "                # so no need to go further\n",
    "                recovered_byte = candidate_byte\n",
    "                break\n",
    "\n",
    "        timings.append((duration, candidate_byte))\n",
    "    else:\n",
    "        # in Python, the \"else\" block of a \"for\" loop\n",
    "        # is executed if the for loop was not exited with a \"break\"\n",
    "        longuest = sorted(timings, reverse=True)[0]\n",
    "        recovered_byte = longuest[1]\n",
    "\n",
    "    return f'{recovered_byte:02x}'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "7a622e53",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "EXPECTED SIGNATURE: 8372506d3a4b11d8c8b02ea9695457a110138849\n",
      "83\n",
      "72\n",
      "50\n",
      "6d\n",
      "3a\n",
      "4b\n",
      "11\n"
     ]
    },
    {
     "ename": "KeyboardInterrupt",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mKeyboardInterrupt\u001b[0m                         Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-10-f128495cad27>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      3\u001b[0m \u001b[0mrecovered_signature\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mstr\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      4\u001b[0m \u001b[0;32mfor\u001b[0m \u001b[0m_\u001b[0m \u001b[0;32min\u001b[0m \u001b[0mrange\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;36m16\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 5\u001b[0;31m     \u001b[0mnext_byte\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mrecover_next_signature_byte\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mwebsite\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mrecovered_signature\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      6\u001b[0m     \u001b[0mrecovered_signature\u001b[0m \u001b[0;34m+=\u001b[0m \u001b[0mnext_byte\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      7\u001b[0m     \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mnext_byte\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-9-8d1332c106c6>\u001b[0m in \u001b[0;36mrecover_next_signature_byte\u001b[0;34m(website, already_recovered)\u001b[0m\n\u001b[1;32m      8\u001b[0m         )\n\u001b[1;32m      9\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 10\u001b[0;31m         \u001b[0mduration\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mmeasure_verification_time\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mcandidate_signature\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mwebsite\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     11\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     12\u001b[0m         \u001b[0;32mif\u001b[0m \u001b[0mtimings\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-5-6738c2b3f610>\u001b[0m in \u001b[0;36mmeasure_verification_time\u001b[0;34m(signature, website)\u001b[0m\n\u001b[1;32m      2\u001b[0m     \u001b[0mstart_time\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mtime\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mperf_counter_ns\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      3\u001b[0m     \u001b[0;32mtry\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 4\u001b[0;31m         \u001b[0mwebsite\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mhandle_query\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34mf\"http://localhost:9000/test?file=foo&signature={signature}\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      5\u001b[0m         \u001b[0;32mraise\u001b[0m \u001b[0mException\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'signature was not rejected'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      6\u001b[0m     \u001b[0;32mexcept\u001b[0m \u001b[0mInvalidSignatureError\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-2-592e1f0cc696>\u001b[0m in \u001b[0;36mhandle_query\u001b[0;34m(self, url)\u001b[0m\n\u001b[1;32m     20\u001b[0m         \u001b[0msig_bytes\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0munhexlify\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0msignature\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     21\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 22\u001b[0;31m         \u001b[0mverify_signature\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0msig_bytes\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mfile\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mencode\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mmac_key\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m<ipython-input-3-930895a813c8>\u001b[0m in \u001b[0;36mverify_signature\u001b[0;34m(signature, data, key)\u001b[0m\n\u001b[1;32m     15\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     16\u001b[0m         \u001b[0;31m# the \"artificial delay\" of 50 milliseconds we are asked to add\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 17\u001b[0;31m         \u001b[0msleep\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;36m0.05\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     18\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     19\u001b[0m     \u001b[0;31m# We don't return anything:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mKeyboardInterrupt\u001b[0m: "
     ]
    }
   ],
   "source": [
    "print('EXPECTED SIGNATURE:', correct_signature)\n",
    "\n",
    "recovered_signature = str()\n",
    "for _ in range(16):\n",
    "    next_byte = recover_next_signature_byte(website, recovered_signature)\n",
    "    recovered_signature += next_byte\n",
    "    print(next_byte)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23872e2d",
   "metadata": {},
   "source": [
    "I interrupted it because it takes forever, but we can see that it works.\n",
    "\n",
    "Why does it take so long? Well to recover one byte we have to try a large number of signatures (at most 255), and because of the “artificial delay” we added, verifying one signature takes quite some time (50 ms per byte until the first wrong byte).\n",
    "\n",
    "As a result, say that we have already recovered 5 bytes and that we are tring to recover the 6th one: we have to send 255 signatures, each taking 5x50 ms to verify (except one that takes 6x50 ms, but we can neglect this), so a total of about 64 seconds, more than a minute. We could make this time lower by reducing the “artificial delay” but then we would increase the chances of errors in the recovery.\n",
    "\n",
    "Actually I added a small optimization in my function `recover_next_signature_byte` so that we don't have to try 255 different signatures each time: as soon as we notice one signature for which verification takes significantly longer than average, we select it as the correct byte. This saves a lot of time when the correct byte has a low value, but if it's `0xff` we'll still have to try every single byte before we get the correct one."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90d3764f",
   "metadata": {},
   "source": [
    "## Takeaways\n",
    "\n",
    "I want to stress a few important things: this challenge does not rely on the fact that SHA1 is broken. We could use any non-broken cryptographic hash function, for instance SHA2, and it would still work. The challenge doesn't rely either on the fact that you can do length-extension attacks on SHA1 and SHA2. We could use hash functions that are not subject to length extension attacks like SHA3 or [BLAKE3](https://github.com/BLAKE3-team/BLAKE3) and the challenge would still work. HMAC prevents length-extension attacks anyway.\n",
    "\n",
    "So what does this challenge relies on? On the fact that using “normal” comparison to check a signature is not safe. Usually, when you want to test if two values are equal, you will do it in the quickest possible way, and it makes sense to exit at the first byte you find that is different. But when it comes to verifying signature, doing so is not safe, and you should use **constant-time comparison** instead. Python provides [`hmac.compare_digest`](https://docs.python.org/3/library/hmac.html#hmac.compare_digest), but you should probably use [the `cryptography` third-party library](https://cryptography.io/en/latest/hazmat/primitives/mac/hmac/) instead. In Go you have [the `crypto/subtle` module](https://golang.org/pkg/crypto/subtle/), etc ..."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
