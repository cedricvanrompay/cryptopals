{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Challenge 18: Implement CTR, the stream cipher mode\n",
    "\n",
    "> The string:\n",
    "> \n",
    ">     L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==\n",
    "> \n",
    "> ... decrypts to something approximating English in CTR mode, which is an AES block cipher mode that turns AES into a stream cipher, with the following parameters:\n",
    "> \n",
    ">       key=YELLOW SUBMARINE\n",
    ">       nonce=0\n",
    ">       format=64 bit unsigned little endian nonce,\n",
    ">              64 bit little endian block count (byte count / 16)\n",
    "> \n",
    "> CTR mode is very simple.\n",
    "> \n",
    "> Instead of encrypting the plaintext, CTR mode encrypts a running counter, producing a 16 byte block of keystream, which is XOR'd against the plaintext.\n",
    "> \n",
    "> For instance, for the first 16 bytes of a message with these parameters:\n",
    "> \n",
    ">     keystream = AES(\"YELLOW SUBMARINE\",\n",
    ">                     \"\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\")\n",
    "> \n",
    "> ... for the next 16 bytes:\n",
    "> \n",
    ">     keystream = AES(\"YELLOW SUBMARINE\",\n",
    ">                     \"\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x01\\x00\\x00\\x00\\x00\\x00\\x00\\x00\")\n",
    "> \n",
    "> ... and then:\n",
    "> \n",
    ">     keystream = AES(\"YELLOW SUBMARINE\",\n",
    ">                     \"\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x02\\x00\\x00\\x00\\x00\\x00\\x00\\x00\")\n",
    "> \n",
    "> CTR mode does not require padding; when you run out of plaintext, you just stop XOR'ing keystream and stop generating keystream.\n",
    "> \n",
    "> Decryption is identical to encryption. Generate the same keystream, XOR, and recover the plaintext.\n",
    "> \n",
    "> Decrypt the string at the top of this function, then use your CTR function to encrypt and decrypt other things.\n",
    "> \n",
    "> **This is the only block cipher mode that matters in good code.** \n",
    "> Most modern cryptography relies on CTR mode to adapt block ciphers into stream ciphers, because most of what we want to encrypt is better described as a stream than as a sequence of blocks. Daniel Bernstein once quipped to Phil Rogaway that good cryptosystems don't need the \"decrypt\" transforms. Constructions like CTR are what he was talking about.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from base64 import b64decode\n",
    "\n",
    "from libmatasano import encrypt_aes_128_block, bxor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For people that never saw the flow chart for CTR mode:\n",
    "\n",
    "![CTR mode](figures/ctr.svg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def aes_128_ctr_keystream_generator(key, nonce):\n",
    "    counter = 0\n",
    "    while True:\n",
    "        to_encrypt = (nonce.to_bytes(length=8, byteorder='little')\n",
    "                     +counter.to_bytes(length=8, byteorder='little'))\n",
    "        keystream_block = encrypt_aes_128_block(to_encrypt, key)\n",
    "        # equivalent to \"for byte in keystream_block: yield byte\"\n",
    "        # for the \"yield\" keyword in Python,\n",
    "        # see https://docs.python.org/3/tutorial/classes.html#generators\n",
    "        yield from keystream_block\n",
    "\n",
    "        counter += 1\n",
    "\n",
    "def aes_128_ctr_transform(msg, key, nonce):\n",
    "    '''does both encryption (msg is plaintext)\n",
    "    and decryption (msg is ciphertext)'''\n",
    "\n",
    "    keystream = aes_128_ctr_keystream_generator(key, nonce)\n",
    "    # by default our 'bxor' function uses the longest string,\n",
    "    # but here the keystream is of unlimited size\n",
    "    # (bytes are generated on-the-fly)\n",
    "    # so I added a parameter to be able to switch to\n",
    "    # \"take the shortest string\"\n",
    "    return bxor(msg, keystream, longest=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b\"Yo, VIP Let's kick it Ice, Ice, baby Ice, Ice, baby \""
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ctxt = b64decode('L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==')\n",
    "aes_128_ctr_transform(ctxt, key=b'YELLOW SUBMARINE', nonce=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** the comment about CTR mode being\n",
    "*the only block cipher mode that matters in good code*\n",
    "is a bit outdated nowadays\n",
    "as new modes are being used,\n",
    "in particular what we call *authenticated encryption* modes\n",
    "(see [Wikipedia:Authenticated encryption][auth-enc]).\n",
    "However one of the most used such modes is GCM wich is very close to CTR.\n",
    "\n",
    "But another authenticated encryption mechanism is what we call\n",
    "*encrypt-then-mac*\n",
    "and it allows to use even CBC mode without the issues we saw in challenge 17.\n",
    "\n",
    "[auth-enc]: https://en.m.wikipedia.org/wiki/Authenticated_encryption"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
